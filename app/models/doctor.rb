class Doctor < ApplicationRecord
  belongs_to :staff
  has_many :patients
end
