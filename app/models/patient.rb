class Patient < ApplicationRecord
  belongs_to :doctor
  has_many :diagnosis
  has_many :bills
end
