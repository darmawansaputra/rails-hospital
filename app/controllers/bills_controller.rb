class BillsController < ApplicationController
    before_action :authenticate
    before_action :set_bill, only: [:show, :update, :destroy]

    def index
        @bills = Bill.all('created_at DESC')

        render json: @bills
    end

    def show
        render json: @bill
    end

    def create
        @bill = Bill.new(bill_params)

        if @bill.save
            render json: @bill, status: :created
        else
            render json: @bill.errors, status: :unprocessable_entity
        end
    end

    def update
        if @bill.update(bill_params)
            render json: @bill
        else
            render json: @bill.errors, status: :unprocessable_entity
        end
    end

    def destroy
        if @bill
            @bill.destroy
        end
    end

    def authenticate 

    end

    private
        def set_bill
            @bill = Bill.find(params[:id])
        end

        def bill_params
            params.require(:bill).permit(:no, :status, :amount, :duedate, :paydate, :patient_id)
        end
  end
  