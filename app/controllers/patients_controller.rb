class PatientsController < ApplicationController
    before_action :authenticate
    before_action :set_patient, only: [:show, :update, :destroy, :diagnosis, :bills]

    def index
        @patients = Patient.all('name ASC')

        render json: @patients
    end

    def show
        render json: @patient
    end

    def create
        @doctor = Doctor.find(params[:docter_id])
        @patient = @doctor.patients.new(patient_params)

        if @patient.save
            render json: @patient, status: :created
        else
            render json: @patient.errors, status: :unprocessable_entity
        end
    end

    def update
        if @patient.update(patient_params)
            render json: @patient
        else
            render json: @patient.errors, status: :unprocessable_entity
        end
    end

    def destroy
        if @patient
            @patient.destroy
        end
    end

    def diagnosis
        render json: @patient.diagnosis
    end

    def bills
        render json: @patient.bills
    end

    def authenticate 

    end

    private
        def set_patient
            @patient = Patient.find(params[:id])
        end

        def patient_params
            params.require(:patient).permit(:name, :gender, :address, :phone, :doctor_id)
        end
  end
  