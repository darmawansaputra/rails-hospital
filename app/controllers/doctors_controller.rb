class DoctorsController < ApplicationController
    before_action :authenticate
    before_action :set_doctor, only: [:show, :update, :destroy]

    def index
        @doctors = Doctor.all('name ASC')

        render json: @doctors
    end

    def show
        render json: @doctor
    end

    def create
        @doctor = Doctor.new(doctor_params)

        if @doctor.save
            render json: @doctor, status: :created
        else
            render json: @doctor.errors, status: :unprocessable_entity
        end
    end

    def update
        if @doctor.update(doctor_params)
            render json: @doctor
        else
            render json: @doctor.errors, status: :unprocessable_entity
        end
    end

    def destroy
        if @doctor
            @doctor.destroy
        end
    end

    def authenticate 

    end

    private
        def set_doctor
            @doctor = Doctor.find(params[:id])
        end

        def doctor_params
            params.require(:doctor).permit(:name, :gender, :address, :designation)
        end
  end
  