Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/test', to: 'testing#test'
  resources :doctors, only: [:index, :show, :create, :update, :destroy]
  resources :patients, only: [:index, :show, :create, :update, :destroy]
  resources :bills, only: [:index, :show, :create, :update, :destroy]

  get '/patients/:id/diagnosis', to: 'patients#diagnosis'
  get '/patients/:id/bills', to: 'patients#bills'
end
 