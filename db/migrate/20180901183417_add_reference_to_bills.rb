class AddReferenceToBills < ActiveRecord::Migration[5.2]
  def change
    add_reference :bills, :patient, foreign_key: true
  end
end
