class CreateDoctors < ActiveRecord::Migration[5.2]
  def change
    create_table :doctors do |t|
      t.string :name
      t.string :gender
      t.string :address
      t.string :designation

      t.timestamps
    end
  end
end
