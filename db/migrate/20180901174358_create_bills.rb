class CreateBills < ActiveRecord::Migration[5.2]
  def change
    create_table :bills do |t|
      t.string :no
      t.integer :status
      t.integer :amount
      t.datetime :duedate
      t.datetime :paydate

      t.timestamps
    end
  end
end
