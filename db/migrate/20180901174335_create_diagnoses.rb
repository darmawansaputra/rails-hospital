class CreateDiagnoses < ActiveRecord::Migration[5.2]
  def change
    create_table :diagnoses do |t|
      t.string :details
      t.string :remark
      t.datetime :diagdate
      t.string :other

      t.timestamps
    end
  end
end
