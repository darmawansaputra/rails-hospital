class AddReferenceToStaffs < ActiveRecord::Migration[5.2]
  def change
    add_reference :staffs, :doctor, foreign_key: true
  end
end
